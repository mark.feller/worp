# Copyright © 2017 T4S Partners
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

APP=worp
VERSION=`git describe --tag 2> /dev/null`

GOCMD=go
SRC=gitlab.com/t4spartners/worp/
LDFLAGS=-ldflags "-X $(SRC)cmd.version=$(VERSION)"
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOINSTALL=$(GOCMD) install
GOGET=$(GOCMD) get -u

NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
WARN_COLOR=\033[33;01m

all: help

help:
	@echo "$(OK_COLOR)==== $(APP) [$(VERSION)]====$(NO_COLOR)"
	@echo "$(WARN_COLOR)- deps$(NO_COLOR)    : go get all dependencies"
	@echo "$(WARN_COLOR)- build$(NO_COLOR)   : build project"
	@echo "$(WARN_COLOR)- install$(NO_COLOR) : install binaries"
	@echo "$(WARN_COLOR)- clean$(NO_COLOR)   : cleanup"

deps:
	@echo "$(OK_COLOR)[$(APP)] Fetching Dependencies$(NO_COLOR)"
	dep ensure

build:
	@echo "$(OK_COLOR)[$(APP)] Building$(NO_COLOR)"
	$(GOBUILD) $(LDFLAGS)

install:
	@echo "$(OK_COLOR)[$(APP)] Installing$(NO_COLOR)"
	$(GOINSTALL) $(LDFLAGS)

clean:
	@echo "$(OK_COLOR)[$(APP)] Cleanup$(NO_COLOR)"
	$(GOCLEAN)
