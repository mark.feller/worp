// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	csm "gitlab.com/t4spartners/csm-go-lib"
)

func parseFields(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if f := qp.Get("fields"); f != "" {
		var s []string
		if err := json.Unmarshal([]byte(f), &s); err == nil {
			q.Project(s...)
		} else {
			q.Project(f)
		}
	}
}

func parseInclude(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if include := qp.Get("include"); include != "" {
		var s []string
		if err := json.Unmarshal([]byte(include), &s); err == nil {
			q.Include(s...)
		} else {
			q.Include(include)
		}
	}
}

func parseLimit(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if limit := qp.Get("limit"); limit != "" {
		if l, err := strconv.Atoi(limit); err == nil {
			q.Limit(l)
		}
	}
}

func parseOrder(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if order := qp.Get("order"); order != "" {
		// switch o := order.(type) {
		// case string:
		addOrder(q, order)
		// case []string:
		//	fmt.Println("order type []string")
		//	for _, ostr := range o {
		//		addOrder(q, ostr)
		//	}
		// }
	}
}

func addOrder(q *csm.Query, order string) {
	o := strings.Split(order, " ")
	if len(o) == 0 {
		return
	} else if len(o) == 1 {
		q.Order(o[0], csm.Asc)
	} else {
		if strings.ToLower(o[1]) == "desc" {
			q.Order(o[0], csm.Desc)
		} else {
			q.Order(o[0], csm.Asc)
		}
	}
}

func parsePage(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if page := qp.Get("page"); page != "" {
		if p, err := strconv.Atoi(page); err == nil {
			q.Page(p)
		}
	}
}

func parsePageSize(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if size := qp.Get("pagesize"); size != "" {
		if s, err := strconv.Atoi(size); err == nil {
			q.PageSize(s)
		}
	}
}

func parseWhere(q *csm.Query, r *http.Request) {
	qp := r.URL.Query()
	if where := qp.Get("where"); where != "" {
		parseWhereString(q, where)
	}
}

func parseWhereString(q *csm.Query, where string) {
	m := map[string]interface{}{}
	if err := json.Unmarshal([]byte(where), &m); err != nil {
		return
	}

	for k, v := range m {
		switch val := v.(type) {
		case string:
			q.Where(k, val, csm.Eq)
		case map[string]interface{}:
			for k2, v2 := range val {
				switch val2 := v2.(type) {
				case string:
					fmt.Println("where setting", k, val2, k2)
					q.Where(k, val2, csm.OperatorFromString(k2))
				}
			}
		}
	}
}
