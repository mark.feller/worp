// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package server

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/client/business_object"
	"gitlab.com/t4spartners/csm-go-lib/client/searches"
	"gitlab.com/t4spartners/csm-go-lib/client/service"
	"gitlab.com/t4spartners/csm/spec"

	"github.com/gorilla/mux"
)

func (a *Worp) HomeHandler(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, "Hello World")
}

func (a *Worp) Info(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, map[string]interface{}{"version": version})
}

func (a *Worp) Docs(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	v := mux.Vars(r)

	c := &spec.Config{
		Server:  a.Config.Server,
		Objects: []string{v["object"]},
	}

	sp := spec.New(c).WithSession(s)
	sp.InitDefinitions()
	if b, err := sp.Swagger.MarshalJSON(); err != nil {
		respondWithError(w, 500, err.Error())
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write(b)
	}
}

func (a *Worp) ObjectSaveAttachment(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	v := mux.Vars(r)

	name := v["filename"]
	size, err := strconv.Atoi(r.Header.Get("content-length"))
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}

	id, err := s.O(v["object"]).SaveAttachment(r.Body, v["id"], name, int32(size))
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		respondWithJSON(w, http.StatusOK, map[string]string{"id": id})
	}
}

// ObjectFindHandler queries CSM and responsed with an array of objects
// Find supports query parameters fields, limit, order, page, pagesize, where
func (a *Worp) ObjectFindHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	// build query
	query := s.O(v["object"]).Find()
	parseFields(query, r)
	parseLimit(query, r)
	parseOrder(query, r)
	parsePage(query, r)
	parsePageSize(query, r)
	parseWhere(query, r)
	parseInclude(query, r)

	var res []map[string]interface{}
	if err := query.All(&res); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		respondWithJSON(w, http.StatusOK, res)
	}
}

func (a *Worp) ObjectFindOneHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	// build query
	query := s.O(v["object"]).Find()
	parseFields(query, r)
	parseOrder(query, r)
	parseWhere(query, r)
	parseInclude(query, r)

	var res map[string]interface{}
	if err := query.One(&res); err != nil {
		switch err.(type) {
		case (*searches.SearchesGetSearchResultsAdHocV1Unauthorized):
			panic(&service.ServiceTokenBadRequest{})
		default:
			respondWithError(w, http.StatusBadRequest, err.Error())
		}
	} else {
		respondWithJSON(w, http.StatusOK, res)
	}
}

func (a *Worp) ObjectSaveHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	var object map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&object); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	if id, err := s.O(v["object"]).Save(object); err != nil {
		switch e := err.(type) {
		case *business_object.BusinessObjectSaveBusinessObjectV1InternalServerError:
			b, _ := json.Marshal(e.Payload)
			respondWithError(w, http.StatusBadRequest, string(b))
		default:
			respondWithError(w, http.StatusBadRequest, err.Error())
		}
		return
	} else {
		respondWithJSON(w, http.StatusOK, map[string]string{"RecID": id})
	}
}

func (a *Worp) ObjectDeleteByIDHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	var res map[string]interface{}
	if err := s.O(v["object"]).Find().Where("RecID", v["id"], csm.Eq).One(&res); err != nil {
		respondWithJSON(w, http.StatusOK, struct{ found bool }{found: false})
	} else {
		respondWithJSON(w, http.StatusOK, struct{ found bool }{found: true})
	}
}

func (a *Worp) ObjectExistsHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	var res map[string]interface{}
	err := s.O(v["object"]).Find().Where("RecID", v["id"], csm.Eq).One(&res)
	respondWithJSON(w, http.StatusOK, map[string]bool{"found": (err != nil)})
}

func (a *Worp) ObjectFindByIDHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	query := s.O(v["object"]).Find()
	parseFields(query, r)
	parseInclude(query, r)

	var res map[string]interface{}
	if err := query.Where("RecID", v["id"], csm.Eq).One(&res); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		respondWithJSON(w, http.StatusOK, res)
	}
}

func (a *Worp) ObjectUpdateByIDHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	// decode body
	var object map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(object); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}

	// set id from path
	object["RecID"] = v["id"]

	if id, err := s.O(v["object"]).Save(object); err != nil {
		switch e := err.(type) {
		case *business_object.BusinessObjectSaveBusinessObjectV1InternalServerError:
			b, _ := json.Marshal(e.Payload)
			respondWithError(w, http.StatusBadRequest, string(b))
		default:
			respondWithError(w, http.StatusBadRequest, err.Error())
		}
		return
	} else {
		respondWithJSON(w, http.StatusOK, map[string]string{"RecID": id})
	}
}

func (a *Worp) ObjectCountHandler(w http.ResponseWriter, r *http.Request) {
	// Get session associated with request
	s, ok := a.Sessions[r.Header.Get("authorization")]
	if !ok {
		log.Println("Session map failed")
		return
	}

	// path parameters
	v := mux.Vars(r)

	// Initialize query
	query := s.O(v["object"]).Find()
	parseWhere(query, r)

	if count, err := query.Count(); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		respondWithJSON(w, http.StatusOK, map[string]int{"count": count})
	}
}
