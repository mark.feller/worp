// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package server

import (
	"encoding/json"
	"net/http"

	csm "gitlab.com/t4spartners/csm-go-lib"

	"github.com/gorilla/mux"
)

var version string

type Worp struct {
	Router   *mux.Router
	CSM      *csm.Session
	Sessions map[string]*csm.Session

	Config *Config
}

func (w *Worp) InitializeRoutes() {
	w.Router.HandleFunc("/", w.HomeHandler)
	w.Router.Handle("/docs/{object}", w.Authorize(w.Docs)).Methods("GET")

	// Object Paths
	obj := w.Router.PathPrefix("/o/{object}").Subrouter()
	obj.Handle("", w.Authorize(w.ObjectFindHandler)).Methods("GET")
	obj.Handle("", w.Authorize(w.ObjectSaveHandler)).Methods("PUT")
	obj.Handle("/count", w.Authorize(w.ObjectCountHandler)).Methods("GET")
	obj.Handle("/findone", w.Authorize(w.ObjectFindOneHandler)).Methods("GET")
	obj.Handle("/{id:[a-zA-Z0-9]*}", w.Authorize(w.ObjectDeleteByIDHandler)).Methods("DELETE")
	obj.Handle("/{id:[a-zA-Z0-9]*}/exists", w.Authorize(w.ObjectExistsHandler)).Methods("GET")
	obj.Handle("/{id:[a-zA-Z0-9]*}", w.Authorize(w.ObjectFindByIDHandler)).Methods("GET")
	obj.Handle("/{id:[a-zA-Z0-9]*}", w.Authorize(w.ObjectUpdateByIDHandler)).Methods("PUT")
	obj.Handle("/{id:[a-zA-Z0-9]*}/Attachments/{filename}", w.Authorize(w.ObjectSaveAttachment)).Methods("POST")
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
